<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoyensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moyens', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id', true);
            $table->timestamps();
            $table->string('moyen');

        });
   

   DB::table('moyens')->insert([
         [
            'moyen'=>'Avion'
         ]
      ]);
   DB::table('moyens')->insert([
         [
            'moyen'=>'Voiture'
         ]
      ]);
   DB::table('moyens')->insert([
         [
            'moyen'=>'Train'
         ]
      ]);
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moyens');
    }
}
