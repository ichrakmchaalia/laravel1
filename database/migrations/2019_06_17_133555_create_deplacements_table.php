<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedeplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deplacements', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id', true);
            $table->timestamps();
            $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->unsignedInteger('id_ville')->unique();
            $table->unsignedInteger('id_moyen')->unique();
            $table->date('datededepart');
            $table->date('datederetour');  
            $table->time('heurededepart');    
            $table->time('heurederetour');                
        });
       
      Schema::table('deplacements', function($table) {
       $table->foreign('id_ville')->references('id')->on('villes')->onDelete('cascade');});
      Schema::table('deplacements', function($table) {
       $table->foreign('id_moyen')->references('id')->on('moyens')->onDelete('cascade');});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deplacements');
    }
}
