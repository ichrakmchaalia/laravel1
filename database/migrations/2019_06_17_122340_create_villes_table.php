<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVillesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('villes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id', true);
            $table->timestamps();
            $table->string('ville');
        });
   

   DB::table('villes')->insert([
         [
            'ville'=>'Tunis'
         ]
      ]);
   DB::table('villes')->insert([
         [
            'ville'=>'Sousse'
         ]
      ]);
   DB::table('villes')->insert([
         [
            'ville'=>'Djerba'
         ]
      ]);
 }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('villes');
    }
}
