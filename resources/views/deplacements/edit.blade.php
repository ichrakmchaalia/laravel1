@extends('base')

@section('main')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit deplacement
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('deplacements.update', $deplacement->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Nom:</label>
          <input type="text" class="form-control" name="nom" value={{ $deplacement->nom }} />
        </div>
        <div class="form-group">
          <label for="prenom">Prenom:</label>
          <input type="text" class="form-control" name="prenom" value={{ $deplacement->prenom }} />
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="text" class="form-control" name="email" value={{ $deplacement->email }} />
        </div>
        <div class="form-group">
          <label for="ville">Ville:</label>
          <input type="text" class="form-control" name="id_ville" value={{ $deplacement->id_ville }} />
        </div>
        <div class="form-group">
          <label for="moyen">Moyen:</label>
          <input type="text" class="form-control" name="id_moyen" value={{ $deplacement->id_moyen }} />
        </div>
        <div class="form-group">
          <label for="datededepart">Date de depart:</label>
          <input type="text" class="form-control" name="datededepart" value={{ $deplacement->datededepart }} />
        </div>
        <div class="form-group">
          <label for="datederetour">Date de retour:</label>
          <input type="text" class="form-control" name="datederetour" value={{ $deplacement->datederetour }} />
        </div>
        <div class="form-group">
          <label for="heurededepart">Heure de depart:</label>
          <input type="text" class="form-control" name="heurededepart" value={{ $deplacement->heurededepart }} />
        </div>
        <div class="form-group">
          <label for="heurederetour">Heure de retour:</label>
          <input type="text" class="form-control" name="heurederetour" value={{ $deplacement->heurederetour }} />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection
