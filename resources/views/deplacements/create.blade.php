@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Ajouter un deplacement</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('deplacements.store') }}">
          @csrf
          <div class="form-group">    
              <label for="nom">Nom:</label>
              <input type="text" class="form-control" name="nom"/>
          </div>

          <div class="form-group">
              <label for="prenom">Prenom:</label>
              <input type="text" class="form-control" name="prenom"/>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email"/>
          </div>
         <div class="form-group">
              <label for="city">Ville :</label>
              <select name="id_ville">
 <?php foreach ($ville as $ville) {
?>
    <option value="<?php echo $ville->id; ?>"><?php echo $ville->ville; ?></option>
 <?php }
?>
</select>
          </div>
          <div class="form-group">
              <label for="city">Ville :</label>
              <select name="id_moyen">
 <?php foreach ($moyen as $moyen) {
?>
    <option value="<?php echo $moyen->id; ?>"><?php echo $moyen->moyen; ?></option>
 <?php }
?>
</select>
          </div>
          <div class="form-group">
              <label for="datededepart">Date de depart:</label>
              <input type="date" class="form-control" name="datededepart"/>
          </div>    
          <div class="form-group">
              <label for="datederetour">Date de retour:</label>
              <input type="date" class="form-control" name="datederetour"/>
          </div>    
          <div class="form-group">
              <label for="heurededepart">Heure de depart:</label>
              <input type="time" class="form-control" name="heurededepart"/>
          </div>    
          <div class="form-group">
              <input type="time" class="form-control" name="heurederetour"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Ajouter</button>
      </form>
  </div>
</div>
</div>
@endsection
