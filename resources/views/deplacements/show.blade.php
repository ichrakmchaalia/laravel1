<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('deplacements') }}">Deplacement</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('deplacements') }}">View All deplacements</a></li>
        <li><a href="{{ URL::to('deplacements/create') }}">Ajouter deplacement</a>
    </ul>
</nav>

<h1>le deplacement de {{ $deplacement->nom }} {{ $deplacement->prenom }}</h1>

    <div class="jumbotron text-center">
        <strong>Nom:</strong> {{ $deplacement->nom }} <br>
        <p> 
            <strong>Prenom:</strong> {{ $deplacement->prenom }}<br>
            <strong>Email:</strong> {{ $deplacement->email }}<br>
            <strong>Ville:</strong> {{ $deplacement->id_ville }}<br>
            <strong>Moyen:</strong> {{ $deplacement->id_moyen }}<br>
            <td>d.depart: {{$deplacement->datededepart}}</td>
            <td>d.retour: {{$deplacement->datederetour}}</td>
            <td>h.depart: {{$deplacement->heurededepart}}</td>
            <td>h.retour: {{$deplacement->heurederetour}}</td>
        </p>
    </div>

</div>
</body>
</html>
