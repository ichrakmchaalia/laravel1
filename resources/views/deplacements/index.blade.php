@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Deplacements</h1>   
<div>
    <a style="margin: 19px;" href="{{ route('deplacements.create')}}" class="btn btn-primary">New deplacement</a>
    </div>  
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nom</td>
          <td>Prenom</td>
          <td>Email</td>
          <td>Ville</td>
          <td>Moyen</td>
          <td>Date de depart</td>
          <td>Date de retour</td>
          <td>Heure de depart</td>
          <td>Heure de retour</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($deplacements as $deplacement)
        <tr>
            <td>{{$deplacement->id}}</td>
            <td>{{$deplacement->nom}}</td>
            <td>{{$deplacement->prenom}}</td>
            <td>{{$deplacement->email}}</td>
            <td>{{$deplacement->id_ville}}</td>
            <td>{{$deplacement->id_moyen}}</td>
            <td>{{$deplacement->datededepart}}</td>
            <td>{{$deplacement->datederetour}}</td>
            <td>{{$deplacement->heurededepart}}</td>
            <td>{{$deplacement->heurederetour}}</td>
            <td>
                <a href="{{ route('deplacements.edit',$deplacement->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('deplacements.destroy', $deplacement->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div> 
</div>
@endsection
