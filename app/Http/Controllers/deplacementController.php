<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\deplacement;
use App\ville;
use App\moyen;

class deplacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deplacements = deplacement::all();
        $ville = ville::all();
        $moyen = moyen::all();

        return view('deplacements.index', compact('deplacements','ville','moyen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $deplacements = deplacement::all();
     $ville = ville::all();
     $moyen = moyen::all();   
     return view('deplacements.create', compact('deplacements','ville','moyen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'email'=>'required'
        ]);

        $deplacement = new deplacement([
            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'email' => $request->get('email'),
            'id_ville' => $request->get('id_ville'),
            'id_moyen' => $request->get('id_moyen'),
            'datededepart' => $request->get('datededepart'),
            'datederetour' => $request->get('datederetour'),
            'heurededepart' => $request->get('heurededepart'),
            'heurederetour' => $request->get('heurederetour')
        ]);
        $deplacement->save();
        return redirect('/deplacements')->with('success', 'deplacement saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $deplacement = deplacement::find($id);

        // show the view and pass the nerd to it
        return view('deplacements.show',compact('deplacement',$deplacement));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deplacement = deplacement::find($id);
        return view('deplacements.edit', compact('deplacement'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'email'=>'required'
        ]);

        $deplacement = deplacement::find($id);
        $deplacement->nom =  $request->get('nom');
        $deplacement->prenom = $request->get('prenom');
        $deplacement->email = $request->get('email');
        $deplacement->id_ville = $request->get('id_ville');
        $deplacement->id_moyen = $request->get('id_moyen');
        $deplacement->datededepart = $request->get('datededepart');
        $deplacement->datederetour = $request->get('datederetour');
        $deplacement->heurededepart = $request->get('heurededepart');
        $deplacement->heurederetour = $request->get('heurederetour');
        $deplacement->save();

        return redirect('/deplacements')->with('success', 'deplacement updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $deplacement = deplacement::find($id);
         $deplacement->delete();

        return redirect('/deplacements')->with('success', 'Deplacements deleted!');
    }
}
