<?php

namespace App\Http\Controllers;

use App\Moyen;
use Illuminate\Http\Request;

class MoyenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Moyen  $moyen
     * @return \Illuminate\Http\Response
     */
    public function show(Moyen $moyen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Moyen  $moyen
     * @return \Illuminate\Http\Response
     */
    public function edit(Moyen $moyen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Moyen  $moyen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moyen $moyen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Moyen  $moyen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Moyen $moyen)
    {
        //
    }
}
