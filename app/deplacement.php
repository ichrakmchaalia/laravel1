<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deplacement extends Model
{
     protected $fillable = [
        'nom',
        'prenom',
        'email',
        'id_ville',
        'id_moyen',
        'datededepart',
        'heurededepart',
        'datederetour',
        'heurederetour'      
    ];

   public function ville(){
         return $this->hasOne(ville::class);
    }
   public function moyen(){
         return $this->hasOne(moyen::class);
    }


}
